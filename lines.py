#!/usr/bin/python3
#Program to count number of tweets per month.

import os
import subprocess 

def list_files():
    """Make list of all .gz files"""
    f_list = [] #list of files
    for f in os.listdir("."):
        if f.endswith(".txt"):
            f_list.append(f)
    return f_list

def count_lines(f_list):
    """Count all the lines in the files""" 
    c = 0
    for f in f_list:
        with open(f,"r") as fi:
            for line in fi:			 		
                c += 1
    return c
                    
def main():
    f_list = list_files()
    c = count_lines(f_list)
           
if __name__ == '__main__':
	main()
